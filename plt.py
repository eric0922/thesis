import numpy as np
import matplotlib.pyplot as plt
# ==========================================
# 1.圓半徑
r = 5.0
# 2.圓心座標
a, b = (5.13, 5.87)
a2, b2 = (3.99, 3.54)
a3, b3 = (4.87, 4.60)
a4, b4 = (6.22, 1.37)
# ==========================================
theta = np.arange(0, 2*np.pi, 0.01)
x = a + r * np.cos(theta)
y = b + r * np.sin(theta)
x2 = a2 + r * np.cos(theta)
y2 = b2 + r * np.sin(theta)
x3 = a3 + r * np.cos(theta)
y3 = b3 + r * np.sin(theta)
x4 = a4 + r * np.cos(theta)
y4 = b4 + r * np.sin(theta)
fig = plt.figure()
axes = fig.add_subplot(111)
axes.plot(x, y)   #BLUE
axes.plot(x2, y2) #ORANGE
axes.plot(x3, y3) #GREEN
axes.plot(x4, y4) #RED
plt.scatter(a, b)
plt.scatter(a2, b2)
plt.scatter(a3, b3)
plt.scatter(a4, b4)
axes.axis('equal')
plt.title('a')
# ==========================================
plt.show()