/************************************************************
 * prob_analysis.c -- program to print out "prob_analysis.c". *
 *                                                          *
 * Author:  Eric                                         *
 *                                                          *
 * Purpose:                                                 *
 *                                                          *
 * Usage:                                                   *
 *                                                          *
 ***********************************************************/

#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	FILE *pFile_HN_probability;
	float d,x,y,y_squre,r_squre,r;
	float x_right,x_left;
	double prob_HN,test;
	double tmp_prob_HN;
	float  tmp_test = 0;
	float  analysis_probability = 0.4134966716; 
	float  difference;
	int    simulation_times = 10000;
	int    point_number =10000;
	int    angle;
	//int    beamwidth = 300;
	int    beamwidth=360;
	srand(time(NULL));
	pFile_HN_probability = fopen("p_hn_probility.txt", "w");
	//float r = 1;
	//printf("%2f\n",r );
	//r = (float)rand()/RAND_MAX;
	r = 1;
	//d = (float)rand()*r/RAND_MAX;
/*----------------------------------simulation------------------------------------------*/
	for(int j = 360; j>= beamwidth ; beamwidth+=1 )
	{
		tmp_prob_HN =0;
		analysis_probability = 0.4134966716;
for (int j = 0; j < simulation_times+1; ++j)
{
	//d = (float)rand()/RAND_MAX;
	//r = (float)rand()/RAND_MAX;
	//d = (float)rand()*r/RAND_MAX;
	d = (j)*(r/simulation_times);
	//d = 1;
	int area0 = 0;
	int area1 = 0;
	int area2 = 0;
	int count = 0;
	angle = rand()%361;
	if(angle<=beamwidth)
	{
		for (int i = 0; i < point_number; ++i)
		{
		x = (float)rand()*(2*r)/RAND_MAX;
		//x = rand()%(int)(2*r+d);
		y = (float)rand()*(2*r)/RAND_MAX-r;
		y_squre = y*y;
		r_squre = r*r;
		x_right = (x-r)*(x-r);
		x_left = (x-r+d)*(x-r+d);
			if (x<r-0.5*d)
			{
				if(x_right+y_squre<=1)
				{
					area0+=1;
					count+=1;
				}
			}
			else if (x>=r-0.5*d && x<=2*r-d)
			{
				
				if(y_squre + x_left > r_squre && x_right+y_squre <=1)
				{
					area1+=1;
					count+=1;
				}
				else if(y_squre<=r_squre - x_right)
				{
					area0+=1;
					count+=1;
				}
			}
			else if (x>2*r-d)
			{
				if(x_right+y_squre<=1)	
				{
					area1+=1;
					count+=1;
				}	
			}

		}
	prob_HN = (float)area1*100/(count);
	prob_HN = (2*d*prob_HN/100)/(r*r);
	//tmp_prob_HN += prob_HN;
	tmp_prob_HN += prob_HN; 
	//printf("No. %d  : The value of D is: %2.5f R is : %2.4f\n",j,d,r );
	//printf("The probability of area 1 (Hidden node) is :%2.4f\n",prob_HN);
	}
}
//prob_HN = tmp_prob_HN/(simulation_times+1);
tmp_prob_HN = tmp_prob_HN/(simulation_times+1);
/*----------------------------------simulation------------------------------------------*/
/*-----------------------------------analysis-------------------------------------------*/
analysis_probability = ((float)beamwidth/360)*analysis_probability;
difference = sqrt((tmp_prob_HN - analysis_probability)*(tmp_prob_HN - analysis_probability))/analysis_probability;
/*-----------------------------------analysis-------------------------------------------*/
printf("|--------------------------------------------------------------------|\n");
printf("|      The value of beamwidth is        :%d degree                  |\n",beamwidth);
printf("|--------------------------------------------------------------------|\n");
printf("|      The probability of simulation is :%2.6f                    |\n",tmp_prob_HN);
printf("|      The probability of analysis   is :%2.6f                    |\n",analysis_probability);
printf("|--------------------------------------------------------------------|\n");
printf("|      The difference between simulation and analysis   is :%2.5f  |\n",difference);
printf("|--------------------------------------------------------------------|\n");
fprintf(pFile_HN_probability, "Beamwidth\tsimulation\tanalysis\tdifference\n");
fprintf(pFile_HN_probability, "%d\t\t%2.6f\t%2.6f\t%2.6f\n", beamwidth, tmp_prob_HN , analysis_probability,difference*100 );

}
fclose(pFile_HN_probability);
}