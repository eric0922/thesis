#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#define PI acos(-1)
const double esp = 1e-6;
bool Sector_determination(
    float cx, float cy, float ux, float uy, float squaredR, float cosTheta,
    float px, float py)
{

    // D = P - C
    float dx = px - cx;
    float dy = py - cy;
 
    // |D|^2 = (dx^2 + dy^2)
    float squaredLength = dx * dx + dy * dy;
 
    // |D|^2 > r^2
    
    if (squaredLength > squaredR){
        //printf("squaredLength > squaredR\n" );
        return false;
    }
 
    // |D|
    float length = sqrt(squaredLength);
 
    // D dot U > |D| cos(theta)
    if(cosTheta == -1)
        if(fabs(squaredLength-squaredR) <=esp)
            return 1;
        else if(squaredLength<squaredR && fabs(squaredLength-squaredR)>esp)
            return 1;
        else 
            return 0;
        //return squaredLength <= squaredR;
    else
        return dx * ux + dy * uy > length * cosTheta;   
}

float change(float number){
    number = (int)(number*100+0.5)/100.0;
    return number;
}

int main(void) {
    FILE *pFile_point;
    pFile_point = fopen("point.txt", "w");
    int   simulation_times = 1000000;
    int   a = 0;
    int   flag[4];                      //0:AP-gNB 1:STA-UE 2:AP-UE 3:STA-gNB
    float radius = 5;
    //float squaredR = radius * radius;
    float size = 11;
    float Theta  = 180;
    float cosTheta = cos(Theta*PI/180);
    float ax,ay,bx,by,cx,cy,dx,dy;
    float Uabx,Uaby,Ucdx,Ucdy,Dab,Dcd;  //Uab(Uabx,Uaby):Direction vector(a to b) Dab:Distance from a to b
    float Ubax,Ubay,Udcx,Udcy;
    float Theta_a,Theta_b,Theta_c,Theta_d;
    int count = 0;
    float cy_up1,cy_up2,cy_low1,cy_low2,cy_up,cy_low;
    double tmp1,tmp2,tmp;
    double HNP,ENP;

    srand(time(NULL));
    //for (radius ; radius < 11; radius += 0.1)
    //{
        float squaredR = radius * radius;
    for (int i = 0; i < simulation_times; ++i){

        for (int i = 0; i < 4; i++)
        {
            flag[i] = 0;
        }
        count = 0;
        tmp1 = 0;
      
        // a:AP b:STA c:gNB d:UE 
        ax = (float)rand()*size/RAND_MAX;
        
        ay = (float)rand()*size/RAND_MAX;
        
        //bx = (float)rand()*2*radius/RAND_MAX+ax-radius;
        bx = ax + radius;

        tmp = sqrt(radius*radius-(bx-ax)*(bx-ax));
        //by = (float)rand()*2*tmp/RAND_MAX+ay-tmp;
        by = ay;

        cx = (float)rand()*2*radius/RAND_MAX+bx-radius;
        cy = sqrt(radius*radius-(bx-cx)*(bx-cx));
        cy = (float)rand()*2*cy/RAND_MAX+by-cy;  
        //dx = (float)rand()*(size-ax+radius)/RAND_MAX+cx-radius;
        dx = (float)rand()*2*radius/RAND_MAX+cx-radius;
    
        tmp = sqrt(radius*radius-(dx-cx)*(dx-cx));
        dy = (float)rand()*2*tmp/RAND_MAX+cy-tmp;
        //printf("a(%2.2f , %2.2f) b(%2.2f , %2.2f) c(%2.2f , %2.2f) d(%2.2f , %2.2f)\n",ax,ay,bx,by,cx,cy,dx,dy );

        Dab  = sqrt((bx-ax)*(bx-ax)+(by-ay)*(by-ay));
        Uabx = (bx - ax)/Dab;
        Ubax = -1*Uabx;
        Uaby = (by - ay)/Dab;
        Ubay = -1*Uaby;
        //printf("Uab(%2.4f , %2.4f)\n",Uabx,Uaby);
        //printf("--------------------------------\n");

        //choose the angle that a should be rotated
        Theta_a = (rand()*(2*Theta)/180*PI)/RAND_MAX-(Theta/180*PI);
        Theta_b = (rand()*(2*Theta)/180*PI)/RAND_MAX-(Theta/180*PI);
        //printf("Theta_a: %2.4f , %2.4f degree\n",Theta_a,Theta_a*180/PI);
        //printf("--------------------------------\n");

        // Uab multiply rotation matrix
        // x' = x*cos(Theta) - y*sin(Theta)
        // y' = x*sin(Theta) + y*cos(Theta)
        Uabx = Uabx*cos(Theta_a) - Uaby*sin(Theta_a);
        Uaby = Uabx*sin(Theta_a) + Uaby*cos(Theta_a);

        Ubax = Ubax*cos(Theta_b) - Ubay*sin(Theta_b);
        Ubay = Ubax*sin(Theta_b) + Ubay*cos(Theta_b);
        //printf("Uab(%2.4f , %2.4f)\n",Uabx,Uaby);
        //printf("--------------------------------\n");


        Dcd  = sqrt((dx - cx)*(dx - cx) + (dy - cy)*(dy - cy));
        Ucdx = (dx - cx)/Dcd;
        Udcx = -1*Ucdx;
        Ucdy = (dy - cy)/Dcd;
        Udcy = -1*Ucdy;
        //printf("Uab(%2.4f , %2.4f)\n",Uabx,Uaby);
        //printf("Uba(%2.4f , %2.4f)\n",Ubax,Ubay);
        //printf("--------------------------------\n");

        Theta_c = (rand()*(2*Theta)/180*PI)/RAND_MAX-(Theta/180*PI);
        Theta_d = (rand()*(2*Theta)/180*PI)/RAND_MAX-(Theta/180*PI);
        //printf("Theta_c: %2.4f , %2.4f degree\n",Theta_c,Theta_c*180/PI);
        //printf("--------------------------------\n");

        Ucdx = Ucdx*cos(Theta_c) - Ucdy*sin(Theta_c);
        Ucdy = Ucdx*sin(Theta_c) + Ucdy*cos(Theta_c);

        Udcx = Udcx*cos(Theta_d) - Udcy*sin(Theta_d);
        Udcy = Udcx*sin(Theta_d) + Udcy*cos(Theta_d);
        //printf("Ucd(%2.4f , %2.4f)\n",Ucdx,Ucdy);
        //printf("--------------------------------\n");

        // if C in Sector A
        if(Sector_determination(ax,ay,Uabx,Uaby,squaredR,cosTheta,cx,cy)>0){
            // if A also in sector C
            if(Sector_determination(cx,cy,Ucdx,Ucdy,squaredR,cosTheta,ax,ay)>0){
                flag[0] = 1;
            }
        }

        if(Sector_determination(cx,cy,Ucdx,Ucdy,squaredR,cosTheta,bx,by)>0){
            if(Sector_determination(bx,by,Ubax,Ubay,squaredR,cosTheta,cx,cy)>0)
            {
                flag[3] = 1;
            }
        }


        if(Sector_determination(ax,ay,Uabx,Uaby,squaredR,cosTheta,dx,dy)>0){
            if (Sector_determination(dx,dy,Udcx,Udcy,squaredR,cosTheta,ax,ay)>0)
            {
                flag[2] = 1;
            }
            
        }
        
        if(Sector_determination(bx,by,Ubax,Ubay,squaredR,cosTheta,dx,dy)>0){
            if(Sector_determination(dx,dy,Udcx,Udcy,squaredR,cosTheta,bx,by)>0){
                flag[1] = 1;
            }
        }
        
        for (int i = 0; i < 4; i++)
        {
            count += flag[i];
        }

        if (flag[0]==0)
        {   
            if(flag[3]==1)
                tmp1++;
            fprintf(pFile_point, "(%2.4f, %2.4f)\n", ax,ay );
                fprintf(pFile_point, "(%2.4f, %2.4f)\n", bx,by );
                fprintf(pFile_point, "(%2.4f, %2.4f)\n", cx,cy );
                fprintf(pFile_point, "(%2.4f, %2.4f)\n", dx,dy );
            //else if(flag[2]==1)
            //    tmp1++; 
            //printf("a(%2.4f , %2.4f) b(%2.4f , %2.4f) c(%2.4f , %2.4f) d(%2.4f , %2.4f)\n",ax,ay,bx,by,cx,cy,dx,dy );
        }
        /*
        if (flag[1]==0)
        {
            if(flag[3]==1)
                tmp1++;
            else if (flag[2]==1)
            {
                tmp1++;
            }
            //printf("a(%2.4f , %2.4f) b(%2.4f , %2.4f) c(%2.4f , %2.4f) d(%2.4f , %2.4f)\n",ax,ay,bx,by,cx,cy,dx,dy );
        }
        */
        //printf("%d\n",count );
        if(count == 0){
            a++;
            //printf("a(%2.4f , %2.4f) b(%2.4f , %2.4f) c(%2.4f , %2.4f) d(%2.4f , %2.4f)\n",ax,ay,bx,by,cx,cy,dx,dy );
        }
        if (count == 1)
        {   
            
            if(flag[0]==1)
                ENP++;
            else if(flag[1]==1)
                ENP++;
            else if(flag[2]==1)
                ENP++;
            else if(flag[3]==1)
                ENP++;
                
            //ENP++;
            //printf("1\n");
        }

        if (count == 2){
            //printf("2\n");
            if(flag[0]==1 && flag[1]==1)
                ENP++; 
            else if(flag[2]==1 && flag[3]==1)
                ENP++;
        }
        /*
        if(count == 3)
        {
            //printf("3\n");
            if(flag[0]==1 && flag[1]==1 && flag[2]==1)  
        }
        */
        if (count ==4)
        {
            //printf("4\n");
        }
        if(tmp1>=1 && count >0){
            HNP++;
            //printf("%d times\n",i );
            //printf("a(%2.2f , %2.2f) b(%2.2f , %2.2f) c(%2.2f , %2.2f) d(%2.2f , %2.2f)\n",ax,ay,bx,by,cx,cy,dx,dy );
        }
    }
    ENP = ENP/(simulation_times-a)*100;
    HNP = HNP/(simulation_times-a)*100;
    //printf("a(%2.4f , %2.4f) b(%2.4f , %2.4f) c(%2.4f , %2.4f) d(%2.4f , %2.4f)\n",ax,ay,bx,by,cx,cy,dx,dy );
    printf("ENP: %2.4f     HNP: %2.4f\n",ENP,HNP);
    printf("s-a = %d\n",a);
    fclose(pFile_point);
//}
    //printf("ENP: %2.4f    HNP: %2.4f\n",tmp1/count,tmp2/count );
}