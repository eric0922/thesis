/************************************************************
 * prob_analysis.c -- program to print out "prob_analysis.c". *
 *                                                          *
 * Author:  Eric                                         *
 *                                                          *
 * Purpose:                                                 *
 *                                                          *
 * Usage:                                                   *
 *                                                          *
 ***********************************************************/

#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	float  x;
	double prob_HN;
	double tmp_prob_HN;
	float  difference;
	float  analysis_probability;
	int    simulation_times = 10000;
	int    point_number =10000;
	int    angle;
	int    beamwidth = 179;
	float  area_probability = (float)beamwidth/360;
	srand(time(NULL));
/*----------------------------------simulation------------------------------------------*/
for (int j = 0; j < simulation_times+1; ++j)
{
	int area0 = 0;
	int area1 = 0;
	{
		for (int i = 0; i < point_number; ++i)
		{
		x = (float)rand()/RAND_MAX;
			if (x<=area_probability)//point in the transmission area of A
			{
					area0+=1;
			}
			else if (x>area_probability)//point not in the transmission area of A
			{
				angle = rand()%361;
				if(angle<=beamwidth)//represent rotation probability
				{
					area1+=1;
				}
			}

		}
	prob_HN = (float)area1/point_number;
	tmp_prob_HN += prob_HN; 
	}
}
tmp_prob_HN = tmp_prob_HN/(simulation_times+1);
/*----------------------------------simulation------------------------------------------*/
/*-----------------------------------analysis-------------------------------------------*/
analysis_probability = area_probability*(1- area_probability);
difference = sqrt((tmp_prob_HN - analysis_probability)*(tmp_prob_HN - analysis_probability))/analysis_probability;
/*-----------------------------------analysis-------------------------------------------*/
printf("|--------------------------------------------------------------------|\n");
printf("|      The value of beamwidth is        :%d degree                  |\n",beamwidth);
printf("|--------------------------------------------------------------------|\n");
printf("|      The probability of simulation is :%2.6f                    |\n",tmp_prob_HN);
printf("|      The probability of analysis   is :%2.6f                    |\n",analysis_probability);
printf("|--------------------------------------------------------------------|\n");
printf("|      The difference between simulation and analysis   is :%2.4f   |\n",difference);
printf("|--------------------------------------------------------------------|\n");

}